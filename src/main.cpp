#include <iostream>
#include "complexo.hpp"

using namespace std;

int main (int argc, char ** argv) {
	Complexo c1, c2, resultado;
	Complexo c3(4, -8), c4(-10, 15);
	cout << "Primeiro número complexo: ";
	c1.entrada();
	cout << "Segundo número complexo: ";
	c2.entrada();

	c1.imprime_numero();
	c2.imprime_numero(); 
        c3.imprime_numero();
        c4.imprime_numero();

	resultado = c1 + c2;
	cout << "c1 + c2: ";
	resultado.imprime_numero();
	resultado = c3 - c4;
        cout << "c3 - c4: ";
        resultado.imprime_numero();


return 0;
} 
