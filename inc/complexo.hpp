#ifndef COMPLEXO_HPP
#define COMPLEXO_HPP

class Complexo {
private:
	float real;
	float imag;
public:
	Complexo() : real(0), imag(0){};
	Complexo(float r, float i) : real(r), imag(i){};

	void entrada();
	void imprime_numero();

	Complexo operator + (Complexo c2);
	Complexo operator - (Complexo c2);

};
#endif
